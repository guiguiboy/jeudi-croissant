<?php
require_once __DIR__.'/silex.phar';
require_once __DIR__.'/ParticipationService.php';

$app = new Silex\Application();

$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path'       => __DIR__.'/views',
    'twig.class_path' => __DIR__.'/vendor/twig/lib',
));

$app['participation'] = $app->share(function () {
    return new ParticipationService('./data/data.txt');
});

$app->get('/', function () use ($app) {
    return $app['twig']->render('index.twig', array(
        'name'           => 'admin',
        'participations' => $app['participation']->getParticipations(),
        'next'           => $app['participation']->getNext(),
        'matrix'         => $app['participation']->getParticipationMatrix(),
    ));
});

$app->get('/add/{name}', function ($name) use ($app) {
	try
    {
		$app['participation']->addParticipant($name);
			return $app['twig']->render('update.twig', array(
		        'name' => $name,
		));
    }
    catch (Exception $e)
    {
		return $app['twig']->render('error.twig', array(
	        'exception' => $e->getMessage(),
	    ));
    }
});

$app->get('/remove/{name}', function ($name) use ($app) {
	try
    {
		$app['participation']->removeParticipant($name);
			return $app['twig']->render('update.twig', array(
		        'name' => $name,
		));
    }
    catch (Exception $e)
    {
		return $app['twig']->render('error.twig', array(
	        'exception' => $e->getMessage(),
	    ));
    }
});

$app->get('/increase/{name}/{count}', function ($name, $count) use ($app) {
    try
    {
		$app['participation']->increaseParticipation($name, $count);
		return $app['twig']->render('update.twig', array(
	        'name' => $name,
	    ));
    }
    catch (Exception $e)
    {
		return $app['twig']->render('error.twig', array(
	        'exception' => $e->getMessage(),
	    ));
    }
});

$app->get('/decrease/{name}/{count}', function ($name, $count) use ($app) {
    try
    {
		$app['participation']->decreaseParticipation($name, $count);
		return $app['twig']->render('update.twig', array(
	        'name' => $name,
	    ));
    }
    catch (Exception $e)
    {
		return $app['twig']->render('error.twig', array(
	        'exception' => $e->getMessage(),
	    ));
    }
});

$app->run();
