<?php
class ParticipationService
{
    public $file     = null;
    public $elements = array();

    public function __construct($file)
    {
        $this->file = $file;
        foreach (file($file) as $line)
	    {
	        $part = explode(':', trim($line));
	        $this->elements[] = array(
		        'name' => $part[0],
		        'count' => $part[1],
            );
	    }
    }

    /**
     * Returns the participations array
     * 
     * @return array
     */
    public function getParticipations()
    {
        return $this->elements;
    }

    /**
     * Returns next person who has to bring the food
     * 
     * @return string
     */
    public function getNext()
    {
    	$count = PHP_INT_MAX;
    	$less  = '';
    	foreach ($this->elements as $element)
    	{
    		if ($element['count'] < $count)
    		{
    			$less = $element['name'];
    			$count = $element['count'];
    		}
    	}
    	return $less;
    }

    /** 
     * Returns a participation matrix
     * 
     * @return array
     */
    public function getParticipationMatrix()
    {
    	$matrix = array();
    	$max    = $this->getMaxParticipations();
    	foreach ($this->elements as $element)
    	{
    		$matrix[$element['name']] = array();
    		$rest                     = $element['count'];
    		for ($i = 0; $i < $max; $i++)
    		{
    			if ($rest > 0)
    			{
    				$matrix[$element['name']][] = '*';
    				$rest--;
    			}
    			else
    			{
    				$matrix[$element['name']][] = '';
    			}
    		}
    	}
    	return $matrix;
    }

    /**
     * Returns the max participation
     * 
     * @return integer
     */
    public function getMaxParticipations()
    {
    	$count = -1 * PHP_INT_SIZE;
    	$less  = '';
    	foreach ($this->elements as $element)
    	{
    		if ($element['count'] > $count)
    		{
    			$count = $element['count'];
    		}
    	}
    	return $count;
    }

    /**
     * Increases participation of a member
     * 
     * @param $name
     * @param $count
     */
    public function increaseParticipation($name, $count)
    {
    	if (!$this->isExistingMember($name))
    	{
    		throw new Exception(sprintf('Member %s does not exist', $name));
    	}
    	
    	$this->setParticipation($name, $count, 'increase');
    	$this->saveFile();
    }

    /**
     * Decreases participation of a member (why ?)
     * 
     * @param $name
     * @param $count
     */
    public function decreaseParticipation($name, $count)
    {
    	if (!$this->isExistingMember($name))
    	{
    		throw new Exception(sprintf('Member %s does not exist', $name));
    	}
    	$this->setParticipation($name, $count, 'decrease');
    	$this->saveFile();
    }
    
    /**
     * Adds a participant
     * Throws an Exception if the name already exists
     * 
     * @param string $name
     */
    public function addParticipant($name)
    {
    	if ($this->isExistingMember($name))
    	{
    		throw new Exception(sprintf('Member %s already exists', $name));
    	}
    	$this->elements[] = array(
    	    'count' => 0,
    	    'name'  => $name,
    	);
    	$this->saveFile();
    }

    /**
     * Removes a participant
     * Throws an Exception if the name does not exist
     * 
     * @param string $name
     */
    public function removeParticipant($name)
    {
    	if (!$this->isExistingMember($name))
    	{
    		throw new Exception(sprintf('Member %s does not exists', $name));
    	}
    	$newElements = array();
    	foreach ($this->elements as $element)
    	{
	    	if ($element['name'] == $name)
	    	    continue;
    		$newElements[] = $element;
    	}
    	$this->elements = $newElements;
    	$this->saveFile();
    }
    
    /**
     * Checks that a membre exists
     * 
     * @param  string $name
     * @return bool
     */
    protected function isExistingMember($name)
    {
    	foreach ($this->elements as $element)
    	{
    		if ($element['name'] == $name)
    		    return true;
    	}
    	return false;
    }

    /**
     * Updates participation
     * 
     * @param  string  $name
     * @param  integer $count
     * @param  string  $type
     */
    protected function setParticipation($name, $count, $type)
    {
    	foreach ($this->elements as &$element)
    	{
    		if ($element['name'] == $name)
    		{
    			if ($type == 'increase')
    			{
    				$element['count'] += $count;
    			}
    			else
    			{
    				$element['count'] -= $count;
    			}
    		}
    	}
    }

    /**
     * Saves elements to the file
     * Throws an Exception if the file is not writeable
     * 
     * @throws Exception 
     */
    protected function saveFile()
    {
    	if (!is_writable($this->file))
    	{
    		throw new Exception('File must be writable !');
    	}
    	$data = '';
    	foreach ($this->elements as $element)
    	{
    		$data .= sprintf("%s:%s\n", $element['name'], $element['count']);
    	}
    	file_put_contents($this->file, $data);
    }
}
